<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Valoraciones $model */

$this->title = 'Update Valoraciones: ' . $model->cod_val;
$this->params['breadcrumbs'][] = ['label' => 'Valoraciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_val, 'url' => ['view', 'cod_val' => $model->cod_val]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="valoraciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
