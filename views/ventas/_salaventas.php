<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
?>
<div class="col-3 ">
    <?= Html::a('<div class="card '.$temporada.'-tres d-flex wrap mb-3">
        <div class="card-body">
            <div class="col-12 p-0">
                <h4 class="text-center wrap p-0">' . $model->cod_sala . '</h4>
            </div>

            <div class="col-12 text-center p-0">
                <h5>Ventas: ' . $model->getVentas()->count() . '</h5>
                    <h7>Visitas:<br><span class="font-weight-bold text-uppercase">' . $model->getVisitas()->count() . '</span></h7>
            </div>
        </div>
    </div>', ['consultarventa', $model->cod_sala], ['class' => 'text-dark custom-link']) ?>
</div>