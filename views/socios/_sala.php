<?php
$aforo_actual = $model->aforo_actual ? $model->aforo_actual : '0';
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="col-2 ">
    <?= Html::a('<div class="card '.$temporada.'-tres d-flex wrap mb-3">
        <div class="card-body">
            <div class="col-12 p-0">
                <h4 class="text-center wrap p-0">' . $model->cod_sala . '</h4>
            </div>

            <div class="col-12 text-center p-0">
                <h6>Aforo: ' . $aforo_actual . '/' . $model->aforo_max . '</h6>
                    <h7>Tematica:<br><span class="font-weight-bold text-uppercase">' . $model->tematica . '</span></h7>
            </div>
        </div>
    </div>', ['reubicarsocio', [$_GET['1'], $model->cod_sala]], ['class' => 'text-dark custom-link']) ?>
</div>