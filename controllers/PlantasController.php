<?php

namespace app\controllers;

use app\models\Plantas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * PlantasController implements the CRUD actions for Plantas model.
 */
class PlantasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Plantas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Plantas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_plantas' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Plantas model.
     * @param int $cod_plantas Cod Plantas
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_plantas)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        return $this->render('view', [
            'model' => $this->findModel($cod_plantas),
        ]);
    }

    /**
     * Creates a new Plantas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = new Plantas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_plantas' => $model->cod_plantas]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Plantas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_plantas Cod Plantas
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_plantas)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = $this->findModel($cod_plantas);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_plantas' => $model->cod_plantas]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Plantas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_plantas Cod Plantas
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_plantas)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $this->findModel($cod_plantas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Plantas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_plantas Cod Plantas
     * @return Plantas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_plantas)
    {
        if (($model = Plantas::findOne(['cod_plantas' => $cod_plantas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
