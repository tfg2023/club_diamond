<?php

use app\models\Salas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii2mod\alert\Alert;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Cocteles';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
$this->registerJs("
    $.alert({
        title: 'Notificación',
        content: 'La acción se ha registrado correctamente.',
    });
");
?>
<!-- < ?= Alert::widget() ?> -->
<div class="cocteles-index">
<div class="alert alert-warning alert-dismissible fade" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-12">

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_venta',
            'layout' => "{items}\n<div class='col-12'>{pager}</div>",
            'options' => [
                'tag' => 'div',
                'class' => 'row',
            ],
            'itemOptions' => [
                'tag' => false,
            ],
        ]) ?>

    </div>

</div>