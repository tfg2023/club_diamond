<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Objetivos $model */

$this->title = $model->cod_objetivo;
$this->params['breadcrumbs'][] = ['label' => 'Objetivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="objetivos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_objetivo' => $model->cod_objetivo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_objetivo' => $model->cod_objetivo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mes',
            'meta',
            'fruto_mensual',
            'meta_lograda',
            'cod_planta',
            'cod_objetivo',
        ],
    ]) ?>

</div>
