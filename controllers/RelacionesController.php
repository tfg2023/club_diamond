<?php

namespace app\controllers;

use app\models\Relaciones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * RelacionesController implements the CRUD actions for Relaciones model.
 */
class RelacionesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Relaciones models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Relaciones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_atencion' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Relaciones model.
     * @param int $cod_atencion Cod Atencion
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_atencion)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        return $this->render('view', [
            'model' => $this->findModel($cod_atencion),
        ]);
    }

    /**
     * Creates a new Relaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = new Relaciones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_atencion' => $model->cod_atencion]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Relaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_atencion Cod Atencion
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_atencion)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = $this->findModel($cod_atencion);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_atencion' => $model->cod_atencion]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Relaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_atencion Cod Atencion
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_atencion)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $this->findModel($cod_atencion)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Relaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_atencion Cod Atencion
     * @return Relaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_atencion)
    {
        if (($model = Relaciones::findOne(['cod_atencion' => $cod_atencion])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
