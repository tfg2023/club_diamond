<?php

use app\models\Cocteles;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
?>

<div style="min-height: 30vh;"></div>
<div class="d-flex justify-content-center">
    <div class="col-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Cambiar Temporada</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-calendar fa-2xl"></i>', ['cambiartemporada'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
    <div class="col-3 offset-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Gestion de <br>Cocteles</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-martini-glass-citrus fa-2xl"></i>', ['consultarcoctel'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
</div>