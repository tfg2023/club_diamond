<?php

/** @var yii\web\View $this */
/** @var string $content */
$isGuest = Yii::$app->user->isGuest;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
switch ($temporada) {
    case "primavera":
        $color = "#006400";
        break;
    case "verano":
        $color = "#191970";
        break;
    case "otoño":
        $color = "#633520";
        break;
    case "invierno":
        $color = "#FFF";
        break;
    default:
        $color = "blue";
        break;
}
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="https://kit.fontawesome.com/30abfd9964.js" crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <style>
        a {
            color: <?=$color?>!important;
        }
    </style>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column <?=$temporada ?>-uno h-100">
    <?php $this->beginBody() ?>

    <header>
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-expand-md navbar-dark bg-dark '.$temporada .'-dos fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                $isGuest ? (['label' => 'Ventas', 'url' => ['/ventas/index']]) : (['label' => 'Cocteles', 'url' => ['/cocteles/index']]),
                $isGuest ? (['label' => 'Socios', 'url' => ['/socios/index']]) : (['label' => 'Objetivos', 'url' => ['/objetivos/index']]),
                $isGuest ? ('') : (['label' => 'Empleados', 'url' => ['/empleados/index']]),
                $isGuest ? (['label' => 'Login', 'url' => ['/site/login']]
                ) : ('<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
        NavBar::end();
        ?>
    </header>

    <main role="main" class="flex-shrink-0">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer class="footer bg-dark <?=$temporada?>-dos mt-auto py-3 text-light">
        <div class="container">
            <p class="float-left">&copy; Club Diamond <?= date('Y') ?></p>
            <p class="float-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>