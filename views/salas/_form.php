<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Salas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="salas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'aforo_max')->textInput() ?>

    <?= $form->field($model, 'aforo_actual')->textInput() ?>

    <?= $form->field($model, 'tematica')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cod_plantas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
