<?php

namespace app\controllers;

use app\models\Empleados;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * EmpleadosController implements the CRUD actions for Empleados model.
 */
class EmpleadosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Empleados models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Empleados::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_empleado' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empleados model.
     * @param int $cod_empleado Cod Empleado
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_empleado)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        return $this->render('view', [
            'model' => $this->findModel($cod_empleado),
        ]);
    }

    /**
     * Creates a new Empleados model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $model = new Empleados();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_empleado' => $model->cod_empleado]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Empleados model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_empleado Cod Empleado
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_empleado)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $model = $this->findModel($cod_empleado);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_empleado' => $model->cod_empleado]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Empleados model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_empleado Cod Empleado
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_empleado)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $this->findModel($cod_empleado)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empleados model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_empleado Cod Empleado
     * @return Empleados the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_empleado)
    {
        if (($model = Empleados::findOne(['cod_empleado' => $cod_empleado])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGestionarempleados()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Empleados::find()
                ->leftJoin(
                    'valoraciones',
                    'empleados.cod_empleado = valoraciones.cod_empleado'
                )
                ->select([
                    'empleados.*',
                    'promedio_valoraciones' => new Expression('AVG(valoraciones.valoracion)')
                ])
                ->groupBy('empleados.cod_empleado')
                ->orderBy(['promedio_valoraciones' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 20, // Especifica la cantidad de empleados a mostrar por página
            ],
            'sort' => [
                'attributes' => [
                    'Valoración' => [
                        'asc' => ['promedio_valoraciones' => SORT_ASC],
                        'desc' => ['promedio_valoraciones' => SORT_DESC],
                    ],
                    // Otros atributos para la ordenación
                    // ...
                ],
            ],
        ]);

        return $this->render('gestionarEmpleados', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRealizarvaloracion()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        // $empleado = $_GET['empleado'];

        $dataProvider = new ActiveDataProvider([
            'query' => Empleados::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_empleado' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('valoraciones/create', [
            'dataProvider' => $dataProvider,
            'empleado' => $empleado,
        ]);
    }

    public function actionSeleccionarempleado()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Empleados::find()
                ->leftJoin(
                    'valoraciones',
                    'empleados.cod_empleado = valoraciones.cod_empleado'
                )
                ->select([
                    'empleados.*',
                    'promedio_valoraciones' => new Expression('AVG(valoraciones.valoracion)')
                ])
                ->groupBy('empleados.cod_empleado')
                ->orderBy(['promedio_valoraciones' => SORT_DESC]),
        ]);

        return $this->render('seleccionarEmpleado', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
