<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Objetivos $model */

$this->title = 'Modificar Objetivo: ' . $model->cod_objetivo;
$this->params['breadcrumbs'][] = ['label' => 'Objetivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Objetivo-' . $model->cod_objetivo, 'url' => ['view', 'cod_objetivo' => $model->cod_objetivo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="objetivos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>