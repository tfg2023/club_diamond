<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valoraciones".
 *
 * @property int $cod_val
 * @property int|null $cod_empleado
 * @property float|null $valoracion
 *
 * @property Empleados $codEmpleado
 */
class Valoraciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valoraciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_empleado'], 'integer'],
            [['valoracion'], 'number'],
            [['cod_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::class, 'targetAttribute' => ['cod_empleado' => 'cod_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_val' => 'Cod Val',
            'cod_empleado' => 'Cod Empleado',
            'valoracion' => 'Valoracion',
        ];
    }

    /**
     * Gets query for [[CodEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEmpleado()
    {
        return $this->hasOne(Empleados::class, ['cod_empleado' => 'cod_empleado']);
    }
}
