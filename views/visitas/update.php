<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Visitas $model */

$this->title = 'Update Visitas: ' . $model->cod_visita;
$this->params['breadcrumbs'][] = ['label' => 'Visitas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_visita, 'url' => ['view', 'cod_visita' => $model->cod_visita]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="visitas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
