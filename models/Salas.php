<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "salas".
 *
 * @property int|null $aforo_max
 * @property int|null $aforo_actual
 * @property string|null $tematica
 * @property int $cod_sala
 * @property int|null $cod_plantas
 *
 * @property Plantas $codPlantas
 * @property Socios[] $codSocios
 * @property Eventos[] $eventos
 * @property Ventas[] $ventas
 * @property Visitas[] $visitas
 */
class Salas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'salas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aforo_max', 'aforo_actual', 'cod_plantas'], 'integer'],
            [['tematica'], 'string', 'max' => 50],
            [['cod_plantas'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::class, 'targetAttribute' => ['cod_plantas' => 'cod_plantas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'aforo_max' => 'Aforo Max',
            'aforo_actual' => 'Aforo Actual',
            'tematica' => 'Tematica',
            'cod_sala' => 'Cod Sala',
            'cod_plantas' => 'Cod Plantas',
        ];
    }

    /**
     * Gets query for [[CodPlantas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPlantas()
    {
        return $this->hasOne(Plantas::class, ['cod_plantas' => 'cod_plantas']);
    }

    /**
     * Gets query for [[CodSocios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSocios()
    {
        return $this->hasMany(Socios::class, ['cod_socios' => 'cod_socio'])->viaTable('visitas', ['cod_sala' => 'cod_sala']);
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::class, ['cod_salas' => 'cod_sala']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['cod_sala' => 'cod_sala']);
    }

    /**
     * Gets query for [[Visitas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitas()
    {
        return $this->hasMany(Visitas::class, ['cod_sala' => 'cod_sala']);
    }
}
