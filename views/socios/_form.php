<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Socios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="socios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DNI')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Color_fav')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Coche')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('<h3><i class="fa-solid fa-person-circle-check"> Registrar</i></h3> ', ['class' => 'btn btn-lg btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
