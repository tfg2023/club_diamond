<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visitas".
 *
 * @property int $cod_visita
 * @property int|null $cod_socio
 * @property int|null $cod_sala
 * @property string|null $fecha
 *
 * @property Salas $codSala
 * @property Socios $codSocio
 */
class Visitas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_socio', 'cod_sala'], 'integer'],
            [['fecha'], 'safe'],
            [['cod_sala'], 'exist', 'skipOnError' => true, 'targetClass' => Salas::class, 'targetAttribute' => ['cod_sala' => 'cod_sala']],
            [['cod_socio'], 'exist', 'skipOnError' => true, 'targetClass' => Socios::class, 'targetAttribute' => ['cod_socio' => 'cod_socios']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_visita' => 'Cod Visita',
            'cod_socio' => 'Cod Socio',
            'cod_sala' => 'Cod Sala',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[CodSala]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSala()
    {
        return $this->hasOne(Salas::class, ['cod_sala' => 'cod_sala']);
    }

    /**
     * Gets query for [[CodSocio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSocio()
    {
        return $this->hasOne(Socios::class, ['cod_socios' => 'cod_socio']);
    }
}
