<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Plantas $model */

$this->title = 'Update Plantas: ' . $model->cod_plantas;
$this->params['breadcrumbs'][] = ['label' => 'Plantas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_plantas, 'url' => ['view', 'cod_plantas' => $model->cod_plantas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
