<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Visitas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="visitas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_socio')->textInput() ?>

    <?= $form->field($model, 'cod_sala')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
