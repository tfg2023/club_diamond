<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $cod_venta
 * @property int|null $cod_sala
 * @property int|null $cod_coctel
 *
 * @property Cocteles $codCoctel
 * @property Salas $codSala
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_sala', 'cod_coctel'], 'integer'],
            [['cod_sala'], 'exist', 'skipOnError' => true, 'targetClass' => Salas::class, 'targetAttribute' => ['cod_sala' => 'cod_sala']],
            [['cod_coctel'], 'exist', 'skipOnError' => true, 'targetClass' => Cocteles::class, 'targetAttribute' => ['cod_coctel' => 'cod_coctel']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_venta' => 'Cod Venta',
            'cod_sala' => 'Cod Sala',
            'cod_coctel' => 'Cod Coctel',
        ];
    }

    /**
     * Gets query for [[CodCoctel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCoctel()
    {
        return $this->hasOne(Cocteles::class, ['cod_coctel' => 'cod_coctel']);
    }

    /**
     * Gets query for [[CodSala]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSala()
    {
        return $this->hasOne(Salas::class, ['cod_sala' => 'cod_sala']);
    }
}
