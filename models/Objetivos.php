<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetivos".
 *
 * @property string|null $mes
 * @property int|null $meta
 * @property int|null $fruto_mensual
 * @property int|null $meta_lograda
 * @property int|null $cod_planta
 * @property int $cod_objetivo
 *
 * @property Plantas $codPlanta
 */
class Objetivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['mes'], 'date', 'format' => 'dd-MM-yyyy'],
            [['mes'], 'required'],
            // [['mes'], 'safe'],
            [['meta', 'fruto_mensual', 'meta_lograda', 'cod_planta'], 'integer'],
            [['cod_planta'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::class, 'targetAttribute' => ['cod_planta' => 'cod_plantas']],
            [['mes'], 'validarMes'],
        ];
    }
    
    public function validarMes($attribute, $params)
    {
        $mes = $this->$attribute;
        if (!is_numeric($mes) || $mes < 1 || $mes > 12) {
            $this->addError($attribute, 'El mes debe ser un número entre 1 y 12.');
        } else {
            $this->$attribute = date('Y') . '-' . $mes . '-01';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        /*$fechaActual = new \DateTime();
$fechaActualString = $fechaActual->format('d-m-y');*/
        return [
            'mes' => 'Mes' /*. $fechaActualString*/,
            'meta' => 'Meta',
            'fruto_mensual' => 'Fruto Mensual',
            'meta_lograda' => 'Meta Lograda',
            'cod_planta' => 'Cod Planta',
            'cod_objetivo' => 'Cod Objetivo',
        ];
    }

    /**
     * Gets query for [[CodPlanta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPlanta()
    {
        return $this->hasOne(Plantas::class, ['cod_plantas' => 'cod_planta']);
    }
}
