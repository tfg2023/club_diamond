<?php

use app\models\Socios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/*
$this->title = 'Socios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="socios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Socios', ['create'], ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Nombre',
            'Apellidos',
            'DNI',
            'Color_fav',
            'Coche',
            //'cod_socios',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cod_socios' => $model->cod_socios]);
                }
            ],
        ],
    ]); ?>


</div>*/ ?>

<div style="min-height: 30vh;"></div>
<div class="d-flex justify-content-center">
    <div class="col-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Reubicar Usuario</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-recycle fa-2xl"></i>', ['seleccionarsocio'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
    <div class="col-3 offset-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Gestionar Socios</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-users-gear fa-2xl"></i>', ['consultarsocio'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
</div>