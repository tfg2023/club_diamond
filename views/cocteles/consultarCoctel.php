<?php

use app\models\Cocteles;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\i18n\Formatter;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];

$formatter = new Formatter;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Cocteles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cocteles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa-solid fa-glass-water-droplet"></i> Crear Coctel', ['create'], ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'nombre',
                'value' => function ($model) {
                    return mb_convert_case($model->nombre, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'refresco',
                'value' => function ($model) {
                    return mb_convert_case($model->refresco, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'alcohol',
                'value' => function ($model) {
                    return mb_convert_case($model->alcohol, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'tipo_copa',
                'value' => function ($model) {
                    return mb_convert_case($model->tipo_copa, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'planta_arom',
                'value' => function ($model) {
                    return mb_convert_case($model->planta_arom, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'precio',
                'value' => function ($model) use ($formatter) {
                    if (!empty($model->precio)) {
                        return $formatter->asCurrency($model->precio, 'EUR');
                    } else {
                        return null;
                    }
                },
            ],
            [
                'attribute' => 'temporada',
                'value' => function ($model) {
                    return mb_strtoupper($model->temporada);
                },
            ],
            [
                'class' => ActionColumn::className(),
                'headerOptions' => ['style' => 'width:8%'],
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cod_coctel' => $model->cod_coctel]);
                }
            ],
        ],
    ]); ?>


</div>