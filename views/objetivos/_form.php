<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Plantas;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Objetivos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="objetivos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mes')->textInput() ?>

    <?= $form->field($model, 'meta')->textInput() ?>

    <?= $form->field($model, 'fruto_mensual')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'meta_lograda')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'cod_planta')->dropDownList(
        \yii\helpers\ArrayHelper::map($plantasDisponibles, 'cod_plantas', 'cod_plantas')
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('<h3><i class="fa-solid fa-clipboard-check"></i> Establecer</h3>', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>