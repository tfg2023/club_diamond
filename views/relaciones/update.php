<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Relaciones $model */

$this->title = 'Update Relaciones: ' . $model->cod_atencion;
$this->params['breadcrumbs'][] = ['label' => 'Relaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_atencion, 'url' => ['view', 'cod_atencion' => $model->cod_atencion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="relaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
