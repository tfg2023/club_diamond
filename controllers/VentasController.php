<?php

namespace app\controllers;

use app\models\Cocteles;
use app\models\Ventas;
use app\models\Salas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * VentasController implements the CRUD actions for Ventas model.
 */
class VentasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Ventas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ventas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_venta' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ventas model.
     * @param int $cod_venta Cod Venta
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_venta)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_venta),
        ]);
    }

    /**
     * Creates a new Ventas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Ventas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_venta' => $model->cod_venta]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ventas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_venta Cod Venta
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_venta)
    {
        $model = $this->findModel($cod_venta);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_venta' => $model->cod_venta]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ventas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_venta Cod Venta
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_venta)
    {
        $this->findModel($cod_venta)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ventas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_venta Cod Venta
     * @return Ventas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_venta)
    {
        if (($model = Ventas::findOne(['cod_venta' => $cod_venta])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSeleccionarsala()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Salas::find(),
        ]);

        return $this->render('seleccionarSala', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionConsultarventa()
    {
        $sala = $_GET['1'];

        $dataProvider = new ActiveDataProvider([
            'query' => Ventas::find()->where(['cod_sala' => $sala]),
            // $modelo = Modelo::find()->where(['nombre' => 'Juan'])->all();
        ]);

        return $this->render('consultarVenta', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSeleccionarcopa()
    {
        $sala = $_GET['1'];

        $json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
        $data = json_decode($json, true);
        $temporada = $data['temporada'];

        $dataProvider = new ActiveDataProvider([
            'query' => Cocteles::find()->where(['like', 'temporada', $temporada]),
        ]);
        return $this->render('seleccionarCopa', [
            'dataProvider' => $dataProvider,
            'sala' => $sala,
        ]);
    }

    public function actionRealizarventa()
    {
        $coctel = $_GET['1'][0];
        $sala = $_GET['1'][1];
        $model = new Ventas();
        $model->cod_coctel = $coctel;
        $model->cod_sala = $sala;
    
        if ($model->save()) {
            return $this->redirect(['seleccionarcopa', $sala,'success' => true]);
        }
    
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionConsultarventas()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Salas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_venta' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('consultarVentas', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
