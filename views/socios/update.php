<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Socios $model */

$this->title = 'Update Socios: ' . $model->cod_socios;
$this->params['breadcrumbs'][] = ['label' => 'Socios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_socios, 'url' => ['view', 'cod_socios' => $model->cod_socios]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="socios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
