<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Relaciones $model */

$this->title = 'Create Relaciones';
$this->params['breadcrumbs'][] = ['label' => 'Relaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="relaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
