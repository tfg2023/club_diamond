<?php

namespace app\controllers;

use app\models\Socios;
use app\models\Salas;
use app\models\Visitas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SociosController implements the CRUD actions for Socios model.
 */
class SociosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Socios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Socios::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_socios' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Socios model.
     * @param int $cod_socios Cod Socios
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_socios)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_socios),
        ]);
    }

    /**
     * Creates a new Socios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Socios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_socios' => $model->cod_socios]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Socios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_socios Cod Socios
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_socios)
    {
        $model = $this->findModel($cod_socios);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_socios' => $model->cod_socios]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Socios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_socios Cod Socios
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_socios)
    {
        $this->findModel($cod_socios)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Socios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_socios Cod Socios
     * @return Socios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_socios)
    {
        if (($model = Socios::findOne(['cod_socios' => $cod_socios])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsultarsocio()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Socios::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_socios' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('consultarSocio', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReubicarsocio()
    {
        $socio=$_GET[1][0];
        $sala=$_GET[1][1];

        // var_dump($socio);
        // var_dump($sala);exit();
        $model = new Visitas();
        $model->cod_socio = $socio;
        $model->cod_sala = $sala;
    
        if ($model->save()) {
            return $this->redirect(['seleccionarsocio','success' => true]);
        }

        return $this->redirect(['visitas/index','success' => false]);
        // return $this->render('consultarSocio', [
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    public function actionSeleccionarsocio()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Socios::find(),

        ]);

        return $this->render('seleccionarSocio', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSeleccionarsala()
    {
        $socio=$_GET[1];

        $dataProvider = new ActiveDataProvider([
            'query' => Salas::find(),
        ]);

        return $this->render('seleccionarSala', [
            'dataProvider' => $dataProvider,
            'socio' => $socio,
        ]);
    }
}
