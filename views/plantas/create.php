<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Plantas $model */

$this->title = 'Create Plantas';
$this->params['breadcrumbs'][] = ['label' => 'Plantas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
