<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cocteles".
 *
 * @property string|null $nombre
 * @property string|null $refresco
 * @property string|null $alcohol
 * @property string|null $tipo_copa
 * @property string|null $planta_arom
 * @property float|null $precio
 * @property string|null $temporada
 * @property int $cod_coctel
 *
 * @property Ventas[] $ventas
 */
class Cocteles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cocteles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'refresco', 'alcohol', 'tipo_copa', 'planta_arom', 'temporada'], 'string', 'max' => 50],
            [['nombre', 'tipo_copa', 'temporada', 'precio'], 'required'],
            [['precio'], 'number', 'numberPattern' => '/^\d{1,13}(\.\d{1,4})?$/'],
            [
                ['temporada'], 'match', 'pattern' => '/^(primavera|verano|otoño|invierno)(-(primavera|verano|otoño|invierno))*$/',
                'message' => 'El campo temporada debe ser uno de los siguientes valores: primavera, verano, otoño o invierno. 
                También puede usar guiones para separarlos. Por ejemplo: primavera-verano-otoño.'
            ],
            [['refresco', 'alcohol'], 'required', 'when' => function ($model) {
                return empty($model->refresco) && empty($model->alcohol);
            }, 'whenClient' => "function (attribute, value) {
                return $('#refresco').val() == '' && $('#alcohol').val() == '';
            }", 'message' => 'Debe ingresar al menos uno de los dos campos.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'refresco' => 'Refresco',
            'alcohol' => 'Alcohol',
            'tipo_copa' => 'Tipo Copa',
            'planta_arom' => 'Planta Aromatica',
            'precio' => 'Precio',
            'temporada' => 'Temporada',
            'cod_coctel' => 'Cod Coctel',
        ];
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['cod_coctel' => 'cod_coctel']);
    }
}
