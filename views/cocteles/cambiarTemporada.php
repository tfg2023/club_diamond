<?php

use app\models\Cocteles;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
?>

<div style="min-height: 30vh;"></div>
<div class="d-flex justify-content-center">
    <div class="col-4">
        <div class="text-center mb-4">
            <h1 class="h3 mb-5 font-weight-bold">La temporada actual es: <?= ucfirst($temporada) ?></h1>
        </div>
        <div class="text-center mb-4">
            <h1 class="h3 mt-5 mb-3 font-weight-normal">Cambiar Temporada</h1>
        </div>
        <!-- <select onchange="cambiarTemporada()" id="temporada">
            <option value="primavera">Primavera</option>
            <option value="verano">Verano</option>
            <option value="otono">Otoño</option>
            <option value="invierno">Invierno</option>
        </select> -->
        <?= Html::beginForm(['establecertemporada', ''], 'get') ?>
        <?= Html::dropDownList('temporada', $temporada, [
            'primavera' => 'Primavera',
            'verano' => 'Verano',
            'otoño' => 'Otoño',
            'invierno' => 'Invierno',
        ], ['class' => 'form-control','id' =>'temporada', 'onchange' => 'cambiarTemporada()']) ?>
        <?= Html::endForm() ?>
        <p>
            <?= Html::a('<i class="fa-solid fa-calendar fa-2xl"></i>', ['establecertemporada', ''], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block', 'id' => 'enlace-temporada']) ?>
        </p>
    </div>
</div>

<script type="text/javascript">
    function cambiarTemporada() {
        var temporada = '';
        temporada = document.getElementById('temporada').value;
        var enlace = document.getElementById('enlace-temporada');
        var arr = enlace.href.split('=');
        enlace.href = arr[0] + '=' + temporada;
    }
</script>