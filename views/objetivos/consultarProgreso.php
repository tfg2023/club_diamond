<?php
use app\models\Plantas;
use app\models\Objetivos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use miloschuman\highcharts\Highcharts;
// use miloschuman\highcharts\SeriesDataHelper;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Progreso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h4 class="mt-5">Selecciona una planta para ver sus objetivos</h4>

    <div class="col-12">
    <?php $form = ActiveForm::begin(['method' => 'post', 'action' => ['consultarprogreso']]); ?>
        <?= Html::dropDownList('planta', null, ArrayHelper::map($plantasDisponibles, 'cod_plantas', 'cod_plantas'), ['prompt' => 'Selecciona una planta', 'class' => 'form-control mb-3', 'onchange' => 'this.form.submit()']) ?>
        <?php ActiveForm::end(); ?>

        <?php if ($codPlanta !== null) : ?>
            <?php
            $porcentajes = Objetivos::find()
            ->select(['porcentaje' => 'COUNT(*)'])
            ->where(['cod_planta' => $codPlanta])
            ->groupBy('mes')
            ->column();
        
        // Obtener los nombres de los meses para el pie chart
        $nombresMeses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        
        // Preparar los datos para el pie chart
        $seriesData = [];
        foreach ($porcentajes as $index => $porcentaje) {
            $seriesData[] = [
                'name' => $nombresMeses[$index],
                'y' => (int)$porcentaje,
            ];
        }
        
        // Configuración del pie chart
        $chartOptions = [
            'chart' => [
                'type' => 'pie',
            ],
            'title' => [
                'text' => 'Porcentaje de Objetivos por Mes',
            ],
            'series' => [
                [
                    'name' => 'Porcentaje de Objetivos',
                    'data' => $seriesData,
                    'size' => '80%',
                    'dataLabels' => [
                        'formatter' => 'js:function () {
                            return this.point.name + ": " + this.y + " %";
                        }',
                    ],
                ],
            ],
        ];
        
        // Renderizar el pie chart
        // echo Highcharts::widget([
        //     'options' => $chartOptions,
        // ]);
            ?>
    <h4 class="mt-5">Selecciona un objetivo para actualizarle</h4>

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_planta',
                'layout' => "{items}\n<div class='col-12'>{pager}</div>",
                'options' => [
                    'tag' => 'div',
                    'class' => 'row',
                ],
                'itemOptions' => [
                    'tag' => false,
                ],
            ]) ?>
        <?php endif; ?>

    </div>

</div>









