<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Relaciones $model */

$this->title = $model->cod_atencion;
$this->params['breadcrumbs'][] = ['label' => 'Relaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="relaciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_atencion' => $model->cod_atencion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_atencion' => $model->cod_atencion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_atencion',
            'cod_socio',
            'cod_empleado',
        ],
    ]) ?>

</div>
