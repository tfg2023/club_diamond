<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Cocteles $model */

$this->title = 'Creación de Coctel';
$this->params['breadcrumbs'][] = ['label' => 'Cocteles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cocteles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
