<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventos".
 *
 * @property string|null $nombre
 * @property int|null $aumento
 * @property string|null $descripcion
 * @property int|null $cod_salas
 * @property int $cod_evento
 *
 * @property Salas $codSalas
 */
class Eventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aumento', 'cod_salas'], 'integer'],
            [['nombre', 'descripcion'], 'string', 'max' => 50],
            [['cod_salas'], 'exist', 'skipOnError' => true, 'targetClass' => Salas::class, 'targetAttribute' => ['cod_salas' => 'cod_sala']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'aumento' => 'Aumento',
            'descripcion' => 'Descripcion',
            'cod_salas' => 'Cod Salas',
            'cod_evento' => 'Cod Evento',
        ];
    }

    /**
     * Gets query for [[CodSalas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSalas()
    {
        return $this->hasOne(Salas::class, ['cod_sala' => 'cod_salas']);
    }
}
