<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Valoraciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<!-- <div class="valoraciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_empleado')->hiddenInput(['value' => $model->cod_empleado])->label(false) ?>

    <?= $form->field($model, 'valoracion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('<h3><i class="fa-solid fa-check-to-slot"></i> Calificar</h3>', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> -->


<div class="valoraciones-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_empleado')->hiddenInput(['value' => $model->cod_empleado])->label(false) ?>

    <div class="form-group">
        <label class="control-label">Valoración</label>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary valoracion-btn" data-valoracion="1">
                <input type="radio" name="Valoraciones[valoracion]" value="1" autocomplete="off"> 1
            </label>
            <label class="btn btn-secondary valoracion-btn" data-valoracion="2">
                <input type="radio" name="Valoraciones[valoracion]" value="2" autocomplete="off"> 2
            </label>
            <label class="btn btn-secondary valoracion-btn" data-valoracion="3">
                <input type="radio" name="Valoraciones[valoracion]" value="3" autocomplete="off"> 3
            </label>
            <label class="btn btn-secondary valoracion-btn" data-valoracion="4">
                <input type="radio" name="Valoraciones[valoracion]" value="4" autocomplete="off"> 4
            </label>
            <label class="btn btn-secondary valoracion-btn" data-valoracion="5">
                <input type="radio" name="Valoraciones[valoracion]" value="5" autocomplete="off"> 5
            </label>
        </div>
        <?= $form->field($model, 'valoracion')->hiddenInput(['class' => 'valoracion-input'])->label(false) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('<h3><i class="fa-solid fa-check-to-slot"></i> Calificar</h3>', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $('.valoracion-btn').click(function() {
        var valoracion = $(this).data('valoracion');
        $('.valoracion-input').val(valoracion);
    });
JS;
$this->registerJs($script);
?>
