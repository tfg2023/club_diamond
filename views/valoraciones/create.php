<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Valoraciones $model */

$this->title = 'Valoración de empleado';
$this->params['breadcrumbs'][] = ['label' => 'Valoraciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valoraciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
