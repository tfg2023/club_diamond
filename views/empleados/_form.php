<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Empleados $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="empleados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rango')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jornada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_fav')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observaciones')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('<h3><i class="fa-solid fa-person-circle-check"></i> Registrar</h3>', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
