<?php

use yii\bootstrap4\Progress;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
$valoraciones = $model->valoraciones;
$total = array_sum(array_column($valoraciones, 'valoracion'));
$media = $total / max(count($valoraciones), 1);
$mediaFormatted = $media;
if (floor($media) == $media) {
    $mediaFormatted = number_format($media, 0);
} else {
    $mediaFormatted = number_format($media, 2);
}

if (empty($valoraciones)) {
    $media = 'Aun no ha recibido valoraciones.';
} else {
    $media = Progress::widget([
        'percent' => $media * 20,
        'label' => $mediaFormatted >= 10 ? $mediaFormatted . '/5' : $mediaFormatted,
        'barOptions' => [
            'class' => 'bg-danger',
            'backgroundColor' => '#f5f5f5',
            'color' => '#000000',
        ],
        'options' => [
            'style' => 'background-color: #e9ecef;',
        ],
    ]);
}

?>
<div class="col-3">
    <?= Html::a(
        '
        <div class="card '.$temporada.'-tres d-flex wrap mb-3">
            <div class="card-body">
                <div class="col-12 p-0">
                    <div class="employee-name text-center wrap">' . mb_convert_case($model->nombre, MB_CASE_TITLE, "UTF-8") . '</div>
                </div>

                <div class="col-12 text-center p-0">
                    <div class="employee-rating">Valoración:<br>' . $media . '</div>
                    
                    <div class="employee-info">
                    Rango: <span class="font-weight-bold text-uppercase">' . $model->rango . '</span> <br>
                    Jornada: <span class="font-weight-bold text-uppercase">' . $model->jornada . '</span> <br>
                    Observaciones: <span class="font-weight-bold text-uppercase">' . $model->observaciones . '</span> <br>
                    </div>
                </div>
            </div>
        </div>
        ',
        ['valoraciones/create', 'empleado' => $model->cod_empleado],
        ['class' => 'text-dark custom-link']
    ) ?>
</div>