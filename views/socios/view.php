<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Socios $model */

$this->title = $model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Socios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="socios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'cod_socios' => $model->cod_socios], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'cod_socios' => $model->cod_socios], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Nombre',
                'visible' => !empty($model->Nombre),
                'value' => function ($model) {
                    return ucwords(mb_strtolower($model->Nombre));
                },
            ],
            [
                'attribute' => 'Apellidos',
                'visible' => !empty($model->Apellidos),
                'value' => function ($model) {
                    return ucwords(mb_strtolower($model->Apellidos));
                },
            ],
            [
                'attribute' => 'DNI',
                'visible' => !empty($model->DNI),
                'value' => function ($model) {
                    return ucwords(mb_strtolower($model->DNI));
                },
            ],
            [
                'attribute' => 'Color_fav',
                'visible' => !empty($model->Color_fav),
                'value' => function ($model) {
                    return ucwords(mb_strtolower($model->Color_fav));
                },
            ],
            [
                'attribute' => 'Coche',
                'visible' => !empty($model->Coche),
                'value' => function ($model) {
                    return ucwords(mb_strtolower($model->Coche));
                },
            ],
            // 'cod_socios',
        ],
    ]) ?>

</div>
