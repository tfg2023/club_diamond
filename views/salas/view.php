<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Salas $model */

$this->title = $model->cod_sala;
$this->params['breadcrumbs'][] = ['label' => 'Salas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="salas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_sala' => $model->cod_sala], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_sala' => $model->cod_sala], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'aforo_max',
            'aforo_actual',
            'tematica',
            'cod_sala',
            'cod_plantas',
        ],
    ]) ?>

</div>
