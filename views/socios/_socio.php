<?php
$sala_actual = $model->getUltimaVisita() ? 'Sala actual: '.$model->getUltimaVisita()->getCodSala()->one()->cod_sala : 'No se encuentra en una sala';
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="col-2 ">
    <?= Html::a('<div class="card '.$temporada.'-tres d-flex wrap mb-3">
        <div class="card-body">
            <div class="col-12 p-0">
                <h4 class="text-center wrap p-0">' . $model->Nombre . '</h4>
            </div>

            <div class="col-12 text-center p-0">
                <h7>' . $sala_actual . '</h7>
                <h7>cod socio' . $model->cod_socios . '</h7>
            </div>
        </div>
    </div>', ['seleccionarsala', $model->cod_socios], ['class' => 'text-dark custom-link']) ?>
</div>