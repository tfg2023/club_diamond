<?php

use app\models\Empleados;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\bootstrap4\Progress;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
// var_dump($dataProvider);exit();
$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa-solid fa-user-plus"></i> Alta Empleado', ['create'], ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'nombre',
                'value' => function ($model) {
                    return mb_convert_case($model->nombre, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'rango',
                'value' => function ($model) {
                    return mb_convert_case($model->rango, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'jornada',
                'value' => function ($model) {
                    return mb_convert_case($model->jornada, MB_CASE_TITLE, "UTF-8");
                },
            ],
            [
                'attribute' => 'Valoración',
                'value' => function ($model) {
                    $valoraciones = $model->getValoraciones()->all();
                    $total = 0;
                    foreach ($valoraciones as $valoracion) {
                        $total += $valoracion->valoracion;
                    }
                    $media = $total / (count($valoraciones) ? count($valoraciones) : 1);
                    $mediaFormatted = $media;
                    if (floor($media) == $media) {
                        $mediaFormatted = number_format($media, 0);
                    } else {
                        $mediaFormatted = number_format($media, 2);
                    }

                    if ($media !== null && !empty($media)) {
                        return Progress::widget([
                            'percent' => $media * 20,
                            'label' => $mediaFormatted >= 10 ? $mediaFormatted . '/5' : $mediaFormatted,
                            'barOptions' => [
                                'class' => 'bg-danger',
                                'backgroundColor' => '#f5f5f5',
                                'color' => '#000000',
                            ],
                            'options' => [
                                'style' => 'background-color: #e9ecef;',
                            ],
                        ]);
                    } else {
                        return 'Aun no ha recibido valoraciones.';
                    }
                },
                'format' => 'raw',
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cod_empleado' => $model->cod_empleado]);
                }
            ],
        ],
    ]); ?>


</div>