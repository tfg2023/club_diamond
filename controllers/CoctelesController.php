<?php

namespace app\controllers;

use app\models\Cocteles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * CoctelesController implements the CRUD actions for Cocteles model.
 */
class CoctelesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Cocteles models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Cocteles::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_coctel' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cocteles model.
     * @param int $cod_coctel Cod Coctel
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_coctel)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        return $this->render('view', [
            'model' => $this->findModel($cod_coctel),
        ]);
    }

    /**
     * Creates a new Cocteles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = new Cocteles();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_coctel' => $model->cod_coctel]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cocteles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_coctel Cod Coctel
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_coctel)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = $this->findModel($cod_coctel);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_coctel' => $model->cod_coctel]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cocteles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_coctel Cod Coctel
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_coctel)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $this->findModel($cod_coctel)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cocteles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_coctel Cod Coctel
     * @return Cocteles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_coctel)
    {
        if (($model = Cocteles::findOne(['cod_coctel' => $cod_coctel])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsultarcoctel()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Cocteles::find(),
        ]);

        return $this->render('consultarCoctel', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCambiartemporada()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Cocteles::find(),
        ]);
        return $this->render('cambiarTemporada', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEstablecertemporada()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);
        
        $temporada = $_GET['1'];

        $data = [
            'temporada' => $temporada,
        ];
        $json = json_encode($data);
        file_put_contents(Yii::getAlias('@app/./temporada.json'), $json);

        return $this->render('cambiarTemporada');
    }
}
