<?php

use yii\bootstrap4\Progress;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\i18n\Formatter;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
$formatter = new Formatter;
$formatter->locale = 'es-ES';
$formatter->dateFormat = 'dd/MM/yyyy';

$meta = $model->meta;
$meta_lograda = $formatter->asPercent($model->fruto_mensual / $model->meta, 2);

?>
<div class="col-3">
    <?= Html::a(
        '
        <div class="card '.$temporada.'-tres d-flex wrap mb-3">
            <div class="card-body">
                <div class="col-12 p-0">
                    <div class="mes-name text-center wrap">' . $formatter->asDate($model->mes) . '</div>
                </div>

                <div class="col-12 text-center p-0">
                    <div class="meta">Meta:<br>' . $meta . '</div>
                    
                    <div class="objetivo-info">
                    Fruto Mensual: <span class="font-weight-bold text-uppercase">' . $model->fruto_mensual . '</span> <br>
                    Meta Lograda: <span class="font-weight-bold text-uppercase">' . $meta_lograda . '</span> <br>
                    </div>
                </div>
            </div>
        </div>
        ',
        ['update', 'cod_objetivo' => $model->cod_objetivo],
        ['class' => 'text-dark custom-link']
    ) ?>
</div>
