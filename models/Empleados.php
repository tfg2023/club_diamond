<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property string|null $rango
 * @property string|null $jornada
 * @property string|null $color_fav
 * @property string|null $nombre
 * @property string|null $observaciones
 * @property int $cod_empleado
 *
 * @property Socios[] $codSocios
 * @property Relaciones[] $relaciones
 * @property Valoraciones[] $valoraciones
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    public function beforeValidate()
    {
        $this->rango = mb_strtolower($this->rango);
        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rango', 'nombre', 'jornada'], 'required'],
            [['rango'], 'validarRango'],
            [['jornada'], 'validarJornada'],
            [['rango', 'jornada', 'color_fav', 'nombre', 'observaciones'], 'string', 'max' => 50],

        ];
    }

    /**
     * Función de validación personalizada para el campo "jornada".
     */
    public function validarJornada($attribute, $params)
    {
        $this->$attribute = mb_strtolower($this->$attribute);
        if (!in_array($this->$attribute, ['completa', 'parcial', 'nocturna', 'reducida', 'sólo festivos'])) {
            $this->addError($attribute, 'El valor del campo Jornada debe ser uno de los siguientes valores: completa, parcial, nocturna, reducida o sólo festivos.');
        }
    }

    /**
     * Función de validación personalizada para el campo "rango".
     */
    public function validarRango($attribute, $params)
    {
        $this->$attribute = mb_strtolower($this->$attribute);
        if (!in_array($this->$attribute, ['empleado', 'supervisor', 'gerente'])) {
            $this->addError($attribute, 'El valor del campo Rango debe ser uno de los siguientes valores: Empleado, Supervisor o Gerente.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rango' => 'Rango',
            'jornada' => 'Jornada',
            'color_fav' => 'Color Fav',
            'nombre' => 'Nombre',
            'observaciones' => 'Observaciones',
            'cod_empleado' => 'Cod Empleado',
        ];
    }

    /**
     * Gets query for [[CodSocios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSocios()
    {
        return $this->hasMany(Socios::class, ['cod_socios' => 'cod_socio'])->viaTable('relaciones', ['cod_empleado' => 'cod_empleado']);
    }

    /**
     * Gets query for [[Relaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelaciones()
    {
        return $this->hasMany(Relaciones::class, ['cod_empleado' => 'cod_empleado']);
    }

    /**
     * Gets query for [[Valoraciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getValoraciones()
    {
        return $this->hasMany(Valoraciones::class, ['cod_empleado' => 'cod_empleado']);
    }
}
