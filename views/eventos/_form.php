<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Eventos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="eventos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'aumento')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cod_salas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
