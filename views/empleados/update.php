<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Empleados $model */

$this->title = 'Modificar Empleado: ' . $model->cod_empleado . ' ('. mb_convert_case($model->nombre, MB_CASE_TITLE, "UTF-8") .')';
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_empleado, 'url' => ['view', 'cod_empleado' => $model->cod_empleado]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empleados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
