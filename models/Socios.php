<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "socios".
 *
 * @property string|null $Nombre
 * @property string|null $Apellidos
 * @property string|null $DNI
 * @property string|null $Color_fav
 * @property string|null $Coche
 * @property int $cod_socios
 *
 * @property Empleados[] $codEmpleados
 * @property Salas[] $codSalas
 * @property Relaciones[] $relaciones
 * @property Visitas[] $visitas
 */
class Socios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'socios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellidos', 'DNI', 'Color_fav', 'Coche'], 'required'],
            [['Nombre', 'Apellidos'], 'string', 'max' => 50],
            [['DNI'], 'string', 'max' => 9],
            [['DNI'], 'match', 'pattern' => '/^[0-9]{8}[A-Za-z]$/'],
            [['Color_fav', 'Coche'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'DNI' => 'Dni',
            'Color_fav' => 'Color Fav',
            'Coche' => 'Coche',
            'cod_socios' => 'Cod Socios',
        ];
    }

    /**
     * Gets query for [[CodEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEmpleados()
    {
        return $this->hasMany(Empleados::class, ['cod_empleado' => 'cod_empleado'])->viaTable('relaciones', ['cod_socio' => 'cod_socios']);
    }

    /**
     * Gets query for [[CodSalas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSalas()
    {
        return $this->hasMany(Salas::class, ['cod_sala' => 'cod_sala'])->viaTable('visitas', ['cod_socio' => 'cod_socios']);
    }

    /**
     * Gets query for [[Relaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRelaciones()
    {
        return $this->hasMany(Relaciones::class, ['cod_socio' => 'cod_socios']);
    }

    /**
     * Gets query for [[Visitas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitas()
    {
        return $this->hasMany(Visitas::class, ['cod_socio' => 'cod_socios']);
    }

    public function getUltimaVisita()
    {
        return $this->hasMany(Visitas::class, ['cod_socio' => 'cod_socios'])
            ->orderBy(['fecha' => SORT_ASC])
            ->limit(1)
            ->one();
    }
}
