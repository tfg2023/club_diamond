<?php

use app\models\Empleados;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

?>

<div style="min-height: 30vh;"></div>
<div class="d-flex justify-content-center">
    <div class="col-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Gestionar Empleados</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-list-check fa-2xl"></i>', ['gestionarempleados'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
    <div class="col-3 offset-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Realizar <br>Valoración</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-chart-pie fa-2xl"></i>', ['seleccionarempleado'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
</div>