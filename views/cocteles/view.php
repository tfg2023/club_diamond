<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Cocteles $model */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Cocteles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cocteles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'cod_coctel' => $model->cod_coctel], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'cod_coctel' => $model->cod_coctel], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            [
                'attribute' => 'refresco',
                'visible' => !empty($model->refresco),
            ],
            [
                'attribute' => 'alcohol',
                'visible' => !empty($model->alcohol),
            ],
            'tipo_copa',
            [
                'attribute' => 'planta_arom',
                'visible' => !empty($model->planta_arom),
            ],
            [
                'attribute' => 'precio',
                'value' => Yii::$app->formatter->asCurrency($model->precio),
            ],
            [
                'attribute' => 'temporada',
                'value' => mb_strtoupper($model->temporada),
            ],
            // 'cod_coctel',
        ],
    ]) ?>

</div>
