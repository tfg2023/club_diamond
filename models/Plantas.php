<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantas".
 *
 * @property int|null $aforo_max
 * @property int|null $aforo_actual
 * @property int $cod_plantas
 *
 * @property Objetivos[] $objetivos
 * @property Salas[] $salas
 */
class Plantas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aforo_max', 'aforo_actual'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'aforo_max' => 'Aforo Max',
            'aforo_actual' => 'Aforo Actual',
            'cod_plantas' => 'Cod Plantas',
        ];
    }

    /**
     * Gets query for [[Objetivos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivos()
    {
        return $this->hasMany(Objetivos::class, ['cod_planta' => 'cod_plantas']);
    }

    /**
     * Gets query for [[Salas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalas()
    {
        return $this->hasMany(Salas::class, ['cod_plantas' => 'cod_plantas']);
    }
}
