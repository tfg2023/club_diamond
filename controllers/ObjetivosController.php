<?php

namespace app\controllers;

use app\models\Objetivos;
use app\models\Plantas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * ObjetivosController implements the CRUD actions for Objetivos model.
 */
class ObjetivosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Objetivos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Objetivos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_objetivo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Objetivos model.
     * @param int $cod_objetivo Cod Objetivo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_objetivo)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        return $this->render('view', [
            'model' => $this->findModel($cod_objetivo),
        ]);
    }

    /**
     * Creates a new Objetivos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        // Obtener las plantas disponibles desde la base de datos
        $plantasDisponibles = Plantas::find()->all();

        $model = new Objetivos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['objetivos/index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'plantasDisponibles' => $plantasDisponibles,
        ]);
    }

    /**
     * Updates an existing Objetivos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_objetivo Cod Objetivo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_objetivo)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $model = $this->findModel($cod_objetivo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['consultarprogreso', 'planta' => $model->cod_planta]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Objetivos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_objetivo Cod Objetivo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_objetivo)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $this->findModel($cod_objetivo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Objetivos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_objetivo Cod Objetivo
     * @return Objetivos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_objetivo)
    {
        if (($model = Objetivos::findOne(['cod_objetivo' => $cod_objetivo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEstablecerobjetivo()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Objetivos::find(),
        ]);

        return $this->render('establecerObjetivo', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionConsultarprogreso()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $plantasDisponibles = Plantas::find()->all();
        $codPlanta = Yii::$app->request->post('planta');

        if (is_null($codPlanta) && isset($_GET['planta'])){
            $codPlanta = $_GET['planta'];
        }

        $query = Objetivos::find();
        if ($codPlanta !== null) {
            $query->where(['cod_planta' => $codPlanta]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('consultarProgreso', [
            'dataProvider' => $dataProvider,
            'plantasDisponibles' => $plantasDisponibles,
            'codPlanta' => $codPlanta,
        ]);
    }
}
