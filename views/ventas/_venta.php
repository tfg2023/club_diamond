<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
$color ='';
?>
<div class="col-3 text-uppercase">
    <?= Html::a('<div class="card '.$temporada.'-tres d-flex wrap mb-3">
        <div class="card-body">
            <div class="col-12 p-0">
                <h4 class="text-center p-0">' . $model->nombre . '</h4>
            </div>

            <div class="col-12 p-0">
                <h5>Receta: <br> </h5><h5 class="text-center">' . $model->alcohol . '<br> + <br>' . $model->refresco . '</h5>
                    <h6 class="text-right">Precio: <span class="font-weight-bold">' . $model->precio . '€</span></h6>
            </div>
        </div>
    </div>', ['realizarventa', [$model->cod_coctel, $_GET['1']]], ['class' => 'text-dark custom-link']) ?>
</div>

<?php
/*
'nombre',
'refresco',
'alcohol',
'tipo_copa',
'planta_arom',
'precio',
'temporada',
'cod_coctel',
*/
?>