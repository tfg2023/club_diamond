<?php

use app\models\Ventas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/*
$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ventas', ['create'], ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_venta',
            'cod_sala',
            'cod_coctel',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cod_venta' => $model->cod_venta]);
                }
            ],
        ],
    ]); ?>


</div>
*/ ?>
<div style="min-height: 30vh;"></div>
<div class="d-flex justify-content-center">
    <div class="col-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Realizar Venta</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-martini-glass fa-2xl"></i>', ['seleccionarsala'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>

    </div>
    <div class="col-3 offset-3">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3 font-weight-normal">Consultar Ventas</h1>
        </div>
        <p>
            <?= Html::a('<i class="fa-solid fa-list fa-2xl"></i>', ['consultarventas'], ['class' => 'btn btn-lg '.$temporada.'-dos btn-dark btn-block']) ?>
        </p>
    </div>
</div>