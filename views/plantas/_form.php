<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];
/** @var yii\web\View $this */
/** @var app\models\Plantas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="plantas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'aforo_max')->textInput() ?>

    <?= $form->field($model, 'aforo_actual')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
