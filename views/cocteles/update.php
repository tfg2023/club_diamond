<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Cocteles $model */

$this->title = 'Modificar Coctel: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Coctel', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'cod_coctel' => $model->cod_coctel]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="cocteles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
