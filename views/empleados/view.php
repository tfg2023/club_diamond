<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Empleados $model */

$this->title = mb_convert_case($model->nombre, MB_CASE_TITLE, "UTF-8");
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="empleados-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'cod_empleado' => $model->cod_empleado], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'cod_empleado' => $model->cod_empleado], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de que desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'rango',
                'visible' => !empty($model->rango),
                'value' => !empty($model->rango) ? mb_convert_case($model->rango, MB_CASE_TITLE, "UTF-8") : null,
            ],
            [
                'attribute' => 'jornada',
                'visible' => !empty($model->jornada),
                'value' => !empty($model->jornada) ? mb_convert_case($model->jornada, MB_CASE_TITLE, "UTF-8") : null,
            ],
            [
                'attribute' => 'color_fav',
                'visible' => !empty($model->color_fav),
                'value' => !empty($model->color_fav) ? mb_convert_case($model->color_fav, MB_CASE_TITLE, "UTF-8") : null,
            ],
            [
                'attribute' => 'nombre',
                'visible' => !empty($model->nombre),
                'value' => !empty($model->nombre) ? mb_convert_case($model->nombre, MB_CASE_TITLE, "UTF-8") : null,
            ],
            [
                'attribute' => 'observaciones',
                'visible' => !empty($model->observaciones),
                'value' => !empty($model->observaciones) ? mb_convert_case($model->observaciones, MB_CASE_TITLE, "UTF-8") : null,
            ],
            // [
            //     'attribute' => 'cod_empleado',
            //     'visible' => !empty($model->cod_empleado),
            //     'value' => !empty($model->cod_empleado) ? mb_convert_case($model->cod_empleado, MB_CASE_TITLE, "UTF-8") : null,
            // ],
            // [
            //     // Add this attribute to show the related model's data
            //     // Replace `Valoraciones` with your related model's name
            //     // Replace `valoraciones` with your related model's attribute name
            //     // Replace `cod_empleado` with your related model's foreign key attribute name
            //     // Replace `nombre` with your related model's attribute name you want to show
            //     // You can add more attributes as needed
            //     'attribute' => 'valoraciones.nombre',
            //     'label' => Yii::t('app', 'Valoraciones'),
            //     // Add this value to show the related model's data using the get method of the related model
            //     // Replace `Valoraciones` with your related model's name
            //     // Replace `cod_empleado` with your related model's foreign key attribute name
            //     // Replace `getValoraciones()` with your related model's get method name you want to use
            //     // Replace `nombre` with your related model's attribute name you want to show
            //     // You can add more attributes as needed
            //     'value' => function ($data) {
            //         $valoraciones = $data->getValoraciones()->all();
            //         $result = '';
            //         foreach ($valoraciones as $valoracion) {
            //             $result .= $valoracion->nombre . ', ';
            //         }
            //         return rtrim($result, ', ');
            //     },
            // ],
        ],
    ]) ?>


</div>