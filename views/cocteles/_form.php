<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$json = file_get_contents(Yii::getAlias('@app/./temporada.json'));
$data = json_decode($json, true);
$temporada = $data['temporada'];

/** @var yii\web\View $this */
/** @var app\models\Cocteles $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="cocteles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'refresco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alcohol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_copa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'planta_arom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precio')->textInput() ?>

    <?= $form->field($model, 'temporada')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa-solid fa-champagne-glasses"></i> Crear', ['class' => 'btn '.$temporada.'-dos btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
