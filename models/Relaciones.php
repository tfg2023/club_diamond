<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relaciones".
 *
 * @property int $cod_atencion
 * @property int|null $cod_socio
 * @property int|null $cod_empleado
 *
 * @property Empleados $codEmpleado
 * @property Socios $codSocio
 */
class Relaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_socio', 'cod_empleado'], 'integer'],
            [['cod_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::class, 'targetAttribute' => ['cod_empleado' => 'cod_empleado']],
            [['cod_socio'], 'exist', 'skipOnError' => true, 'targetClass' => Socios::class, 'targetAttribute' => ['cod_socio' => 'cod_socios']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_atencion' => 'Cod Atencion',
            'cod_socio' => 'Cod Socio',
            'cod_empleado' => 'Cod Empleado',
        ];
    }

    /**
     * Gets query for [[CodEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEmpleado()
    {
        return $this->hasOne(Empleados::class, ['cod_empleado' => 'cod_empleado']);
    }

    /**
     * Gets query for [[CodSocio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSocio()
    {
        return $this->hasOne(Socios::class, ['cod_socios' => 'cod_socio']);
    }
}
