<?php

use app\models\Empleados;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Elige el empleado que quieres valorar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-12">

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_empleado',
            'layout' => "{items}\n<div class='col-12'>{pager}</div>",
            'options' => [
                'tag' => 'div',
                'class' => 'row',
            ],
            'itemOptions' => [
                'tag' => false,
            ],
        ]) ?>

    </div>

</div>