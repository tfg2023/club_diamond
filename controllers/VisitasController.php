<?php

namespace app\controllers;

use app\models\Visitas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * VisitasController implements the CRUD actions for Visitas model.
 */
class VisitasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Visitas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        // if(Yii::$app->user->isGuest)
        // return $this->redirect(['site/login']);

        $dataProvider = new ActiveDataProvider([
            'query' => Visitas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_visita' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Visitas model.
     * @param int $cod_visita Cod Visita
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_visita)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        return $this->render('view', [
            'model' => $this->findModel($cod_visita),
        ]);
    }

    /**
     * Creates a new Visitas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = new Visitas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_visita' => $model->cod_visita]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Visitas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_visita Cod Visita
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_visita)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $model = $this->findModel($cod_visita);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_visita' => $model->cod_visita]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Visitas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_visita Cod Visita
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_visita)
    {
        if(Yii::$app->user->isGuest)
        return $this->redirect(['site/login']);

        $this->findModel($cod_visita)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Visitas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_visita Cod Visita
     * @return Visitas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_visita)
    {
        if (($model = Visitas::findOne(['cod_visita' => $cod_visita])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
